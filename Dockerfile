FROM debian:sid
RUN apt-get update && apt-get upgrade -y && apt-get dist-upgrade -y
RUN apt-get install -yy git build-essential bzip2 ca-certificates cmake git gnupg2 libc6-dev libfile-fcntllock-perl libfontconfig-dev libicu-dev liblzma-dev liblzo2-dev libsdl1.2-dev libsdl2-dev libxdg-basedir-dev make software-properties-common tar wget xz-utils zlib1g-dev
